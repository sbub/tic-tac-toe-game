import React, { useState } from 'react';
import Game from 'components/Game';
import BoardColor from 'components/BoardColor';
import './App.css';
import ThemeContext from 'contexts/theme';

const App = () => {
  const [theme, setTheme] = useState('light')
  return (
    <ThemeContext.Provider value={[theme, setTheme]}>
      <Game />
      <BoardColor />
    </ThemeContext.Provider>
  );
}

export default App;
