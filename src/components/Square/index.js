import React, {useContext} from 'react';
import ThemeContext from 'contexts/theme';

const Square = ({ value, onClick}) => {
  const [theme] = useContext(ThemeContext);
  return (
    <button
      className="square"
      onClick={() => onClick()}
      style={theme === 'dark' ? {background: 'darkblue', color: 'white'} : {}}
    >{value}</button>
)};

export default Square;
