import React, { useContext } from 'react';
import ThemeContext from 'contexts/theme';

const BoardColor = () => {
  const [theme, setTheme] = useContext(ThemeContext);
  return (
    <div>
      <p>Selected board color: {theme}</p>
      <button onClick={() => setTheme('dark')}>dark</button>
      <button onClick={() => setTheme('light')}>light</button>
    </div>
  )
}

export default BoardColor;
