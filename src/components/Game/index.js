import React, { useState } from 'react';
import Board from 'components/Board';
import calculateWinner from 'helpers/calculateWinner';

const Game = () => {
  const [xIsNext, setXisNext] = useState(true);
  const [history, setHistory] = useState([{
    squares: Array(9).fill(null)
  }]);
  const [stepNumber, setStepNumber] = useState(0);

  const current = history[stepNumber];
  const winner = calculateWinner(current.squares);

  const moves = history.map((step, move) => {
    const desc = move ?
      'Go to move #' + move :
      'Go to game start';
    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{desc}</button>
      </li>
    );
  });

  let status;
  if(winner) {
    status = 'Winner ' + winner;
  } else {
    status = `Next player: ${xIsNext ? 'X' : 'O'}`;
  }

  // 1. add new history object with updated squares
  // 2. change xIsNext to false;
  // 3. update squares array with clicked value
  const handleClick = (i) => {
    // const history = this.state.history.slice(0, this.state.stepNumber + 1);
    // copy the history array;
    const historyCopy = history.slice();
    const current = historyCopy[historyCopy.length - 1];
    const squares = current.squares.slice();
    // in case winner is already defined or there is a value in a cell
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = xIsNext ? 'X' : 'O';
    setHistory(historyCopy.concat([{
      squares: squares,
    }]));
    setStepNumber(historyCopy.length);
    setXisNext(!xIsNext);
  }

  const jumpTo = (step) => {
    setStepNumber(step);
    setXisNext((step%2) === 0);
  }

  return (
    <div className="game">
      <div className="game-board">
        <Board
          squares={current.squares}
          onClick={(i) => handleClick(i)}
        />
      </div>
      <div className="game-info">
        <div>{status}</div>
        <div>{moves}</div>
      </div>
    </div>
  )
}

export default Game;
